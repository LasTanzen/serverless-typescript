## Usage

- Run debug mode (watch ts + run offline server): `npm run debug`
- Start only serverless offline: `npm start`
- Build typescript: `npm run build`
- Build and watch typescript: `npm run watch`
- Deploy all functions: `npm run deploy`

### TODO:
- Add chokidar or similar to restart offline server when changes in serverless.yml
