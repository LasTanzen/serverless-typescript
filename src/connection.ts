import { Sequelize } from 'sequelize-typescript';

export const initSequelize = () => {
  const sequelize = new Sequelize({
    name: 'sequel',
    dialect: 'mysql',
    username: 'root',
    password: '',
    modelPaths: [__dirname + '/models']
  });
}
