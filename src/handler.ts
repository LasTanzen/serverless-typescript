import { Context, Callback } from 'aws-lambda';

import { User } from './models/User';
import { initSequelize } from './connection';

initSequelize();

export const create = (event: any, context: Context, callback: Callback) => {
  const body: User = JSON.parse(event.body);
  const user = new User(body);
  user.save()
  .then(user => {
      const response = {
        statusCode: 200,
        body: JSON.stringify(user)
      };
      callback(undefined, response);
  }, err => callback(err));
};

export const get = (event: any, context: Context, callback: Callback) => {
  User.findAll<User>()
  .then(users => {
    const response = {
      statusCode: 200,
      body: JSON.stringify(users)
    };
    callback(undefined, response);
  }, err => callback(err));
};
